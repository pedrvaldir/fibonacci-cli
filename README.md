Especialização em Desenvolvimento Ágil de Software 2021

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*Infra-estrutura para desenvolvimento e implantação de Software (DevOps)*

Prof. Alexander Robert Kutzke

# Exercício Pipelines

Esse exercício pode ser feito em dupla.

Inicie fazendo o Fork desse repositório.

Este exercício é composto de 4 passos principais:

1) Implementação de uma aplicação de linha de comando, utilizando node.js, que exibe o N-ésimo número da sequência de fibonacci (`n` é passado como argumento). Exemplo de execução:

```sh
$ fibonacci-cli -n 10
34
```

Utilize a seguinte aplicação como exemplo: https://gitlab.com/das-alexkutzke/roman-numerals

2) Escreva testes unitários para essa aplicação utilizando o Jest;

3) Crie um Gitlab Pipeline com as seguintes características:

  - Um *job* `test` que executa a rotina de testes unitário:
    - Esse job deve ser executado tanto para commits na branch main quanto para merge requests;
  * Um job `publish` que publica sua aplicação no registry.npmjs.org.

4) Simule um Merge-request na aplicação para que o pipeline do MR seja testado. Não conclua o MR. Deixe-o aberto para que o professor possa avaliá-lo depois.

## Sobre a publicação de pacotes no Registry NPMJS

Para publicar seu pacote no Registry NPMJS, será necessário criar o seu próprio usuário: https://www.npmjs.com/signup

Uma vez criado seu usuário, será importante criar um Token de acesso com permissão para publicação (Opção `Access Tokens`). Esse token deve ser utilizado no pipeline para que não seja necessário digitar diretamente sua senha. O comando para registrar o token no seu pacote é (na pasta que contém seu `package.json`):

```sh
npm set //registry.npmjs.org/:_authToken SEU_TOKEN
```

Para publicar, basta executar:

```sh
npm publish
```

Atenção a dois detalhes importantes:

1) Dentro do arquivo `package.json` troque o nome da aplicação para `seu_usuario-fibonacci-cli` (para evitar conflito com publicações de seus colegas;
2) O npm não deixará que a publicação seja feita se a versão da aplicação não for alterada no arquivo `package.json`.
