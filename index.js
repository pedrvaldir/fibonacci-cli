#!/usr/bin/env node

const yargs = require('yargs');
const Fibonacci = require('./fibonacci.js');

module.exports = () => {
  const options = yargs
   .usage("Usage: -n <fibonacci number>")
   .option("n", { alias: "fibonacci-number", describe: "value", type: "int", demandOption: true })
   .argv;

  const rn = new Fibonacci(options.n);
    
  console.log(rn.toInt());
}