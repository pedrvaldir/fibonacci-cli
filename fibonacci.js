class Fibonacci {
  constructor(number) {
    this.number = number;
    this.result = this.#result();
  }

  toInt() {
    return (this.result);
  }

  #result() {
    var values = []
    values[0] = 0
    values[1] = 1

    for (var i = 2; i < this.number; i++) {
      values[i] = values[i - 2] + values[i - 1]
    }
    return (values[values.length - 1]);
  }
}

module.exports = Fibonacci;
